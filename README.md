# Kafka Connect CDC Training

## [Course 01 - Kafka & Kafka Connect](https://gitlab.com/rich04230/kafka-connect-cdc-training/-/tree/master/course01)
![Kafka File Source & Sink Connector](images/course01.png)

## [Course 02 - Kafka Connect CDC](https://gitlab.com/rich04230/kafka-connect-cdc-training/-/tree/master/course02)
![Kafka Connect CDC](images/course02.png)
