## Course 01 - Kafka & Kafka Connect
![Kafka File Source & Sink Connector](../images/course01.png)

### [slide](https://drive.google.com/file/d/1WiiBaTUZ0-50PPI4RpKJJ-ysH6pGUk2B/view?usp=sharing)

### Kafka Broker

- Start zookeeper
```bash
./bin/zookeeper-server-start.sh -daemon config/zookeeper.properties
```

- Start Kafka
```bash
./bin/kafka-server-start.sh -daemon config/server.properties
```

- Get broker’s id
```bash
./bin/zookeeper-shell.sh localhost:2181 ls  /brokers/ids | tail -n1
```
- Describe the broker with id
```bash
./bin/zookeeper-shell.sh localhost:2181 get  /brokers/ids/0
```

- Create a topic (log-events)
```bash
bin/kafka-topics.sh  \
--create --topic log-events  \
--bootstrap-server localhost:9092 \
--replication-factor 1  \
--partitions 2
```

- Describe the topic
```bash
./bin/kafka-topics --describe --zookeeper localhost:2181 --topic log-events
```

### Kafka Connect
- Start Kafka Connect worker
```bash
./bin/connect-distributed.sh -daemon config/connect-distributed.properties
```
- Get all connectors
```bash
curl localhost:8083/connectors | jq
```
- Running fileSource connector
```bash
curl -d @"connect-file-source.json" \
  -H "Content-Type: application/json" \
  -X POST http://localhost:8083/connectors
```

- Running fileSink connector
```bash
curl -d @connect-file-sink.json \
  -H "Content-Type: application/json" \
  -X POST http://localhost:8083/connectors
```

- Running Kafka Connect UI
```bash
docker run --rm -it -p 8000:8000 \
           -e "CONNECT_URL={your connector url}" \
           landoop/kafka-connect-ui
```
